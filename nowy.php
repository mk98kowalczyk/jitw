
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php
include 'menu.php';
?>

<?php   

if(!isset($_POST['login'])){
    header('Location: index.php');
    exit();
}

session_start();
$login=$_POST['login'];
$name = $_POST['name'];
$password1 = $_POST['password1'];
$password2 = $_POST['password2'];
$describe = $_POST['describe'];


if(strlen($login)<3 || strlen($login)>12){
    $_SESSION['wronglogin'] = "Login musi miec miedzy 3 a 12 znakow!";

    header('Location: rejestracja.php');
    exit();
}

if (ctype_alnum($login)==false)
		{
			
            $_SESSION['wronglogin']="Nick może składać się tylko z liter i cyfr (bez polskich znaków)";
            header('Location: rejestracja.php');
            exit();
		}

if(file_exists($name)){
    $_SESSION['wrongname']="Nazwa blogu jest juz zajęta!";
        header('Location: rejestracja.php');
        exit();
}


if ((strlen($password1)<6) || (strlen($password1)>20))
	{
		
        $_SESSION['wrongpass']="Hasło musi posiadać od 6 do 20 znaków!";
        header('Location: rejestracja.php');
        exit();
	}
if($password1 != $password2){
    $_SESSION['wrongpass'] = "Hasła nie sa identyczne!";

    header('Location: rejestracja.php');
    exit();
}

if(strlen($describe)<10 || strlen($describe)>101){
    $_SESSION['wrongdescribe'] = "Opis musi miec przynajmniej 10 do maksymalenie 100 znaków!";

    header('Location: rejestracja.php');
    exit();
}

if (!isset($_POST['regulamin']))
		{
			
            $_SESSION['e_regulamin']="Potwierdź akceptację regulaminu!";
            header('Location: rejestracja.php');
            exit();
		}			



$password_hashed = md5($password1);


if (!file_exists($name)) {
    mkdir($name, 0755, true);

    $sciezka_pliku_txt = $name . "/info.txt";
    $plik = fopen($sciezka_pliku_txt, 'w');

    if (flock($plik, LOCK_EX)) {
       fputs($plik, $login . "\n");
       fputs($plik, $password_hashed."\n");
       fputs($plik, $describe);
       echo "Blog został stworzony <br />Możesz przejść do kolejnych kroków!";
    }

    flock($plik, LOCK_UN);
    fclose($plik);

 }



// echo "Wszystko ok!\n";

?>
    
</body>
</html>