
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wpis</title>
</head>
<body>

<?php
session_start();

if(!isset($_POST['login'])){
    header('Location: index.php');
    exit();
}
include 'menu.php';


$login = $_POST['login'];
$password = $_POST['password'];
$content = $_POST['content'];
$data = $_POST['data'];
$time = $_POST['time'];


$user = False; //czy znaleziono uzytkownika
$folder = new RecursiveDirectoryIterator('.');

// Autentykacja
$pathToBlog = NULL;
foreach (new RecursiveIteratorIterator($folder) as $pathToFile => $file) {
    if (! ($file->isDir())) {
        if ($file->getFileName() == 'info.txt') {
            $lines = file($pathToFile);

            $loginInFile = $lines[0];   //dane w pliku info.txt
            $loginInFile = rtrim($loginInFile, "\r\n");
            $passwordInFile = $lines[1];
            $passwordInFile = rtrim($passwordInFile, "\r\n");

            if ($login == $loginInFile) {    //sprawdzenie loginu i hasła
                if (md5($password) == $passwordInFile) {
                    $user = True;
                    $pathToBlog = $file->getPath();
                    break;
                }
            }
        }
    }
}
if (!$user) {
    $_SESSION['wronglogin'] = "Podane hasło lub nazwa użytkownika są nieprawidlowe!\n";
    header('Location: dodajWpis.php');
    exit();
}

if(strlen($content)<10){
    $_SESSION['wrongcontent'] = "wpis musi miec przynajmniej 10 znaków!";

    header('Location: dodajWpis.php');
    exit();
}

if(strlen($data)!=10){
    $_SESSION['wrongdata'] = "Wprowadz date w poprawnym formacie";
    
    header('Location: dodajWpis.php');
    exit();
}
if(strlen($time)!=5){
    $_SESSION['wrongtime'] = "Wprowadz czas w poprawnym formacie";

    header('Location: dodajWpis.php');
    exit();
}

if($user){ // gdy odnajdziemy i zweryfikujemy uzytkownika
    //tworzymy nazwe dla naszego pliku
        $data = str_replace("-", "", $data);
		$time = str_replace(":", "", $time);
		$sekundy = date("s");
		$id = 0;

		do {
			$tmpId = sprintf("%02d", $id);
			$nameOfFIle = $data.$time.$sekundy.$tmpId;
			$pathToFileWithContent = "./".$pathToBlog."/".$nameOfFIle;
			$id = $id + 1;
		} while (file_exists($pathToFileWithContent));

        // zapisujemy nasz plik
		$file = fopen($pathToFileWithContent, 'w');
		fputs($file, $content);
        fclose($file);
        
        //dodajemy załączniki
		$attachments = array();
		for ($i = 1 ; $i <= sizeof($_FILES) ; $i++) {
			$nameOfAttachment = 'attachment' . $i;
			$currentAttachment = $_FILES[$nameOfAttachment];
			array_push($attachments, $currentAttachment);
        }
        

        $numberAttachment = 1;
        foreach($attachments as $attachment) {
			$expectedFolder = "./" . $pathToBlog . "/";
            $rozszerzenie = pathinfo($attachment['name'], PATHINFO_EXTENSION);
            // $rozszerzenie="ooo";  
			$expectedFile = $expectedFolder . $data . $time . $sekundy .
			$tmpId . $numberAttachment . "." . $rozszerzenie;

			//dodawanie pliku
			if (file_exists($expectedFile)) {
                $_SESSION['wrongfile'] ="Plik " . $attachment['name'] . "juz istnieje! <br />";
               
                header('Location: dodajWpis.php');
                exit();
			} else {
				if (move_uploaded_file($attachment["tmp_name"], $expectedFile)) {
					echo "Plik " . $attachment['name'] . " został dołączony do wpisu <br />";
				}
			}
			$numberAttachment = $numberAttachment + 1;
        }
        
     

}





?>
    
    
</body>
</html>