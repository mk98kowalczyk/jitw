<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

</head>
<body>

<?php
include 'menu.php';

if(!isset($_POST['nick'])){
    header('Location: index.php');
    exit();
}
session_start();

$nick=$_POST['nick'];
$typeOfComent=$_POST['typeComent'];
$content = $_POST['content'];
$selected=$_POST['selected'];

if(strlen($nick)<3 || strlen($nick)>12){
    $_SESSION['wrongnick'] = "Nick musi miec miedzy 3 a 12 znakow!";

    header('Location: dodajKomentarz.php');
    exit();
}
if (ctype_alnum($nick)==false)
		{
			
            $_SESSION['wrongnick']="Nick może składać się tylko z liter i cyfr (bez polskich znaków)";
            header('Location: dodajKomentarz.php');
            exit();
		}

if(strlen($content)<10 || strlen($content)>101){
        $_SESSION['wrongcontent'] = "komentarz musi miec przynajmniej 10 do maksymalenie 100 znaków!";
        
        header('Location: dodajKomentarz.php');
        exit();
    }

//Wszytsko jest ok mozna dodac komentarz

// szukamy gdzie jest plik z wpisem
   $folder = new RecursiveDirectoryIterator('.');
   $folderWithBlog = NULL;
   $fileBlog = NULL;
   foreach (new RecursiveIteratorIterator($folder) as $pathToFile => $file) {
      if (! ($file->isDir())) {
       if (basename($file) == $selected) {
          $fileBlog = $file;
          $folderWithBlog = dirname($file);
         }
      }
   }



   if (!file_exists($fileBlog . ".k")) {  
    mkdir($fileBlog.".k", 0755, true); //tworzymy katalog z nazwa .k na koncu -> komentarze
 }

 $coments = $fileBlog.".k/";

 $index = 0;
   while (file_exists($coments."/".$index)) {
      $index = $index + 1;
   }

   //dodaj komentarz
   $pathToComent = $coments . "/" . $index;
   $fileComent = fopen($pathToComent, "w");
   fputs($fileComent, $typeOfComent . "\n");
   $time = date("Y-m-d H:i:s");
   fputs($fileComent, $time . "\n");
   fputs($fileComent, $nick . "\n");
   fputs($fileComent, $content);
   fclose($fileComent);

   echo '<h2>Dodanie komentarza zakończone!</h2>';

?>
    
</body>
</html>