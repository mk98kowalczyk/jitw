<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dodawanie Komentarza</title>
    <style>
		.error
		{
			color:red;
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>
</head>
<body>

<?php
session_start();
include 'menu.php';
?>

<h1>Dodaj Komentarz!</h1>

<form action="koment.php" method="post" enctype="multipart/form-data">

        <div class="nick">
        <p>Podaj swoj nick:</p>  
        <input type="text" name="nick"></div>
        <?php
       
         if(isset($_SESSION['wrongnick']))
            echo '<div class="error">'.$_SESSION['wrongnick'].'</div>';
            unset($_SESSION['wrongnick']);
         ?>

        <div class="selected">
            <p>Wybierz wpis który chcesz skomentować </p>
        <select name="selected"><br />
              <?php
					  if (isset($_GET['selectComent'])) {
						  $selectComent = $_GET['selectComent'];
					  } else {
						  $selectComent = "";
					  }

	              $folder = new RecursiveDirectoryIterator('.');
	              foreach (new RecursiveIteratorIterator($folder) as $pathToFile => $file) {
	                 if (! ($file->isDir())) {
	                   if (preg_match("/\d{16}$/", $file)) {
								 if (rtrim($selectComent) == rtrim($file)) {
									 echo "<option selected>" . basename($file) . "</option>";
								 } else {
	                      	echo "<option>" . basename($file) . "</option>";
							 	 }
	                   }
	                }
	             }
             ?>
          </select>

        </div>

        <div class="content">
        <p>Podaj treść komentarza: </p>
            <textarea name="content"  cols="50" rows="10"></textarea><br />
            </div>
            <?php
       
       if(isset($_SESSION['wrongcontent']))
          echo '<div class="error">'.$_SESSION['wrongcontent'].'</div>';
          unset($_SESSION['wrongcontent']);
       ?>

        <div class="typeComent">
            Wybierz typ komentarza:
            <select name="typeComent">
              <option>Pozytywny</option>
              <option>Neutralny</option>
              <option>Negatywny</option>
            </select>
            </div>
        <input type="reset" value="Wyczyść" name="wyczysc" />
        <input type="submit" value="Dodaj komentarz!">



</form>

    
</body>
</html>