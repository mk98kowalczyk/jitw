<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dodaj Wpis</title>
    <style>
		.error
		{
			color:red;
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>
</head>
<body>

<?php
include 'menu.php';
?>

<h1>Wypełnij formularz i dodaj wpis do swojego blogu!</h1>


<form  action="wpis.php" method="post" enctype="multipart/form-data">
        <div class="login">
        <p>Podaj login uzytkownika:</p>  
        <input type="text" name="login"></div>
        <?php
       
         if(isset($_SESSION['wronglogin']))
            echo '<div class="error">'.$_SESSION['wronglogin'].'</div>';
            unset($_SESSION['wronglogin']);
         ?>
        

        <div class="password">
        <p>Powtorz hasło:</p>
         <input type="password" name="password"></div>
       
        
        <div class="content"> 
        <p>Co chcesz napisać?</p> 
        <textarea name="content" id="content" cols="100" rows="20"></textarea></div>
        <?php
			if (isset($_SESSION['wrongcontent']))
			{
				echo '<div class="error">'.$_SESSION['wrongcontent'].'</div>';
				unset($_SESSION['wrongcontent']);
			}
		?>	
        </div>

        <div class="data">
        <p>Podaj datę w formacie[RRRR-MM-DD]:</p> 
        		<input id="data" type="text" name="data"  >
        </div>
        <?php
			if (isset($_SESSION['wrongdata']))
			{
                echo '<div class="error">'.$_SESSION['wrongdata'].'</div>';
                
                unset($_SESSION['wrongdata']);
                
			}
		?>	

        <div class="time">
        <p> Podaj godzinę w formacie[GG:MM]:</p>
        		<input id="time" type="text" name="time" >
				
        </div>
        <?php
			if (isset($_SESSION['wrongtime']))
			{
				echo '<div class="error">'.$_SESSION['wrongtime'].'</div>';
				unset($_SESSION['wrongtime']);
			}
		?>	

        <div class="attachments">
            <p>Dodaj załącznik:</p>
        <input type="file" name="attachment1">
        <input type="file" name="attachment2">
        <input type="file" name="attachment3">
        </div>
        <?php
			if (isset($_SESSION['wrongfile']))
			{
				echo '<div class="error">'.$_SESSION['wrongfile'].'</div>';
				unset($_SESSION['wrongfile']);
			}
		?>	
        
       

        <input type="reset" value="Wyczyść!" name="wyczysc" />
        <input type="submit" value="Dodaj wpis!">


    
    </form>
    
</body>
</html>