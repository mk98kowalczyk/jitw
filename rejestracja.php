<?php
session_start();
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>rejestracja nowego bloga</title>

    <style>
		.error
		{
			color:red;
			margin-top: 10px;
			margin-bottom: 10px;
		}
	</style>
</head>
<body>
    
<?php
include 'menu.php';
?>


    <h1>Wypełnij formularz i załóż swoj własny blog!</h1>

    <form  action="nowy.php" method="post">
        <div class="login">
        <p>Podaj login uzytkownika:</p>  
        <input type="text" name="login"></div>
        <?php
        //  session_start();
         if(isset($_SESSION['wronglogin']))
            echo '<div class="error">'.$_SESSION['wronglogin'].'</div>';
            unset($_SESSION['wronglogin']);
         ?>
        

        <div class="name">
        <p>Podaj nazwe blogu: </p>
        <input type="text" name="name"></div>
        <?php
        //  session_start();
         if(isset($_SESSION['wrongname']))
            echo '<div class="error">'.$_SESSION['wrongname'].'</div>';
            unset($_SESSION['wrongname']);
         ?>

        <div class="password1">
        <p>Podaj hasło:</p>
        <input type="password" name="password1"></div>
        
        <div class="password2">
        <p>Powtorz hasło:</p>
         <input type="password" name="password2"></div>
         <?php
        //  session_start();
         if(isset($_SESSION['wrongpass']))
            echo '<div class="error">'.$_SESSION['wrongpass'].'</div>';
            unset($_SESSION['wrongpass']);
         ?>
        
        <div class="describe"> 
        <p>W kilku słowach opisz o czym będzie twój blog:</p> 
        <textarea name="describe" id="describe" cols="40" rows="5"></textarea></div>
        <?php
			if (isset($_SESSION['wrongdescribe']))
			{
				echo '<div class="error">'.$_SESSION['wrongdescribe'].'</div>';
				unset($_SESSION['wrongdescribe']);
			}
		?>	
        </div>
        
        <div class="regulamin">
        <label>
			<input type="checkbox" name="regulamin" /> Akceptuję regulamin
		</label>
        <?php
			if (isset($_SESSION['e_regulamin']))
			{
				echo '<div class="error">'.$_SESSION['e_regulamin'].'</div>';
				unset($_SESSION['e_regulamin']);
			}
		?>	
        </div>

        <input type="reset" value="Wyczyść!" name="wyczysc" />
        <input type="submit" value="Załóż blog!">


    
    </form>

    
</body>
</html>