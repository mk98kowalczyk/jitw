<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Spis blogów</title>
</head>
<body>

<?php
include 'menu.php';

$name = "";
if (isset($_GET['name'])) {
    $name = $_GET['name'];
}

//pierwszy przypadek , bez podania name -> wyswietl wszystko 
if ($name == "") {
    
       $folder = new DirectoryIterator(".");
       echo "<h2>Spis blogów:</h2>";
       echo "<ol>";
    foreach ($folder as $file) {
        if ($file->isDir() && !$file->isDot()) {
           $blog = $file->getFilename();
        if($blog!=".git" )  //&& $blog!=".k"
           echo sprintf("<li><a href=\"blog.php?name=%s\">%s</a></li>", $blog, $blog);
        }
    }
   echo "</ol>";
}else {//drugi przypadek szukamy bloga o podanej nazwie 
    
       $isExist = false;
       $folder = "./" . $name . "/";
       if (file_exists($folder)) {
           $isExist = true;
           echo "\n".'<div class="infoBlog">';
           echo "<h1>Tytuł bloga: " . $name . "</h1>\n";
           $descibeBlog = fopen($folder . "/info.txt", 'r');
           $numberOfLine = 1;
           while (($line = fgets($descibeBlog)) !== false) {
               if ($numberOfLine == 1) {
                   echo "<h2>Autor bloga: " . $line . "</h2>\n";
               } else if ($numberOfLine == 3) {
                   echo "<h3>Opis bloga: " . $line . "</h3>\n";
               }
              
               $numberOfLine = $numberOfLine + 1;
           }
           
           fclose($descibeBlog);
           echo '<div >';


           

           // wybranie wpisów
           $pattern = '/\\d{16}$/';
           $iterator = new DirectoryIterator($folder);
           foreach ($iterator as $currentFile) {
               if (!$currentFile->isDir() && preg_match($pattern, $currentFile)){
                   $content = file_get_contents($iterator->getPathName());
                   echo '<div class="wpis"><h4>Wpis:  '. $currentFile .' </h4>'."\n";
                   echo "<p><b>Treść wpisu:</b> ".$content . "</p></div>\n";

                   echo '<div class="zalaczniki">';
                   // zalaczniki
                   $patternAttachment = '/'.$currentFile.'[1-3]/';
                   echo "<p><b>Załączniki:</b></p> \n";
                   echo "<ul>";
                   foreach (new DirectoryIterator($folder) as $file) {
                       if (preg_match($patternAttachment, $file)) {
                           $pathToComent = $folder;
                           echo sprintf('<li><a href="./%s/%s">%s</a></li>'."\n", $name, $file, $file);
                        }
                   }
                   echo "</ul></div>\n";

                   
                   // wyswietla komentarze
                   if (file_exists($folder . $currentFile . ".k")) {
                    echo '<div class="komentarze">';
                       foreach (new DirectoryIterator($folder . $currentFile . ".k") as $plk) {
                           if(!$plk->isDot() && !$plk->isDir()){
                              
                              $fileComent = fopen($plk->getPathName(), 'r');
                               $numberOfLine = 1;
                               while (($line = fgets($fileComent)) !== false) {
                                   if ($numberOfLine == 1) {
                                       echo "<p><b>Typ komentarza:</b> ".$line."</p>\n";
                                   } else if ($numberOfLine == 2) {
                                       echo "<p><b>Data komentarza:</b> ".$line."</p>\n";
                                   } else if ($numberOfLine == 3) {
                                       echo "<p><b>Autor komentarza:</b> ".$line."</p>\n";
                                    }
                                    else if($numberOfLine>=4){
                                        echo "<p><b>Treść:</b> ".$line."</p>\n";
                                    }
                                
                                   $numberOfLine = $numberOfLine + 1;
                               }
                               fclose($fileComent);
                               echo "<br />";

                           }
                       }
                       echo "</div>\n";
                   }
               }
           }
 }

   if (!$isExist) {
       echo "<h2>Blog o podanej nazwie nie istnieje! </h2>";
   }
}

?>
    
</body>
</html>